//
// Created by sheeran on 23-8-24.
//

#include "PdfApplication.h"
#include <QFileOpenEvent>

PdfApplication::PdfApplication(int argc, char **argv) : QGuiApplication(argc, argv) {

}

bool PdfApplication::event(QEvent *e) {
    if (e->type() == QEvent::FileOpen) {
        QFileOpenEvent *foEvent = static_cast<QFileOpenEvent *>(e);
        m_fileOpener->setProperty("source", foEvent->url());
    }
    return QGuiApplication::event(e);
}
