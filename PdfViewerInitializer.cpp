#include "PdfViewerInitializer.h"

PdfViewerInitializer::PdfViewerInitializer() {
    QString appDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString dir = FileUtils::joinPaths(QStringList() << appDir << DIR::path);

    this->_root = dir;
    this->_viewer = FileUtils::joinPaths(QStringList() << dir << "viewer.html");
    this->_initialized = false;
}

void PdfViewerInitializer::initialize() {
    QtWebView::initialize();
    qmlRegisterType<WebSocketTransport>("PdfViewComponents", 1, 0, "WebSocketTransport");
    qmlRegisterSingletonType<PdfViewerInitializer>("PdfViewComponents", 1, 0, "PdfViewerInitializer", &PdfViewerInitializer::getQmlInstance);

    outlineModel = new QStandardItemModel;
    qmlRegisterSingletonInstance("PdfViewComponents", 1, 0, "OutlineModel", outlineModel);
}

QObject *PdfViewerInitializer::getQmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine) {
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);
    return PdfViewerInitializer::getInstance();
}

QByteArray PdfViewerInitializer::pdfToBase64(const QString &path) {
    if(!FileUtils::exists(path)){
        qCritical() << "Path not found:" << path;
        return QByteArray();
    }
    QString filePath;
    if (path.startsWith("file://")) {
#ifdef _WIN32
        filePath = path.sliced(8);
#else
        filePath = path.sliced(7);
#endif
    } else {
        filePath = path;
    }
    QFile input(filePath);
    if (input.open(QIODevice::ReadOnly)) {
        QByteArray base64 = input.readAll().toBase64();
        return base64;
    }
    return QByteArray();
}

bool PdfViewerInitializer::initializeViewer(bool force) {
    if(!this->_initialized || force){
        FileUtils::removeDir(this->_root);

        bool copied = FileUtils::copyDirs(":/pdfjs", this->_root);
        bool notify = _initialized != copied;
        if (notify) {
            this->_initialized = copied;
            if(this->_initialized) {
                emit viewerChanged();
            }
        }
    }
    return this->_initialized;
}

void PdfViewerInitializer::setOutline(QJsonArray jsonArray) {
    QHash<int, QByteArray> roles;
    roles[0] = "title";
    roles[1] = "dest";
    outlineModel->setItemRoleNames(roles);

    for (int i = 0; i < jsonArray.count(); ++i) {
        QJsonValue value = jsonArray.at(i);
        if (value.isObject()) {
            QJsonObject obj = value.toObject();
            QStandardItem* item = new QStandardItem;
            parseOutlineItem(item, &obj);
            outlineModel->appendRow(item);
        }
    }
}

void PdfViewerInitializer::parseOutlineItem(QStandardItem* item, QJsonObject* obj) {
    // title
    if (!obj->contains("title")) {
        return;
    }
    QJsonValue jsonValue = obj->value("title");
    if (jsonValue.isString()) {
        QString title = jsonValue.toString();
        item->setData(title, 0);
    }
    // dest
    if (obj->contains("dest")) {
        jsonValue = obj->value("dest");
        if (jsonValue.isString()) {
            item->setData(jsonValue.toString(), 1);
        } else if (jsonValue.isArray()) {
            QJsonArray arr = jsonValue.toArray();
            QJsonDocument document(arr);
            item->setData(document.toJson(QJsonDocument::Compact), 1);
        }
    }
    // items
    if (!obj->contains("items")) {
        return;
    }
    jsonValue = obj->value("items");
    if (jsonValue.isArray()) {
        QJsonArray items = jsonValue.toArray();
        for (int i = 0; i < items.count(); ++i) {
            QJsonValue value = items.at(i);
            if (value.isObject()) {
                QJsonObject childObject = value.toObject();
                QStandardItem* child = new QStandardItem;
                parseOutlineItem(child, &childObject);
                item->appendRow(child);
            }
        }
    }
}
