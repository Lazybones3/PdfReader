import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs


ApplicationWindow {
    id: root
    width: 600
    height: 800
    visible: true
    title: qsTr("Read Book")
    property string source

    header: ToolBar {
        RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                action: contentAction
            }

            Label {
                Layout.fillWidth: true
            }

            ToolButton {
                action: optionsMenuAction
                Menu {
                    id: optionsMenu
                    topMargin: root.header.height
                    x: parent.width - width

                    Action {
                        text: qsTr("Open")
                        shortcut: StandardKey.Open
                        onTriggered: fileDialog.open()
                    }

                    Action {
                        text: qsTr("Exit")
                        onTriggered: Qt.quit()
                    }
                }
            }
        }
    }

    Action {
        id: contentAction
        icon.name: "drawer"
        onTriggered: drawer.open()
    }

    Drawer {
        id: drawer
        width: 0.33 * root.width
        height: root.height - root.header.height
        topMargin: root.header.height

        PdfOutline {
            id: pdfOutline
            width: parent.width
            height: parent.height

            onPageSelected: function(page) {
                console.log(page)
                pdfView.goToDestination(page)
            }
        }

        onOpened: {
            pdfView.loadOutline()
        }
    }

    Action {
        id: optionsMenuAction
        icon.name: "menu"
        onTriggered: optionsMenu.open()
    }

    PdfView {
        id: pdfView
        width: parent.width
        height: parent.height

        onViewerLoaded: {
            // Load pdf only when viewer is ready
            // pdfView.load("path/to/my/document.pdf")
            console.log("onViewerLoaded")
        }

        onPdfLoaded: {
            // Pdf has been correctly loaded
            console.log("onPdfLoaded")
        }

        onError: function (message) {
           // Some error occurred
           console.error("Error: ", message)
        }

        onTextSelected: function (txt) {
           console.log("onTextSelected: ", txt)
        }
    }

    FileDialog {
        id: fileDialog
        title: "Open a PDF file"
        nameFilters: [ "PDF files (*.pdf)" ]
        onAccepted: {
            var filePath = selectedFile.toString()
            pdfView.load(filePath)
        }
    }
}
