#ifndef PDFREADER_SINGLETON_CPP
#define PDFREADER_SINGLETON_CPP

#include "Singleton.h"

template <class T>
T* Singleton<T>::_instance = nullptr;

template<class T>
T *Singleton<T>::getInstance() {
    static QMutex mutex;
    if (_instance == nullptr) {
        mutex.lock();
        if (_instance == nullptr) {
            _instance = new T();
        }
        mutex.unlock();
    }
    return _instance;
}

template<class T>
void Singleton<T>::deleteInstance() {
    static QMutex mutex;
    mutex.lock();
    delete _instance;
    _instance = nullptr;
    mutex.unlock();
}

#endif