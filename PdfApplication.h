//
// Created by sheeran on 23-8-24.
//

#ifndef PDFREADER_PDFAPPLICATION_H
#define PDFREADER_PDFAPPLICATION_H

#include <QGuiApplication>
#include <QObject>

class PdfApplication : public QGuiApplication {
public:
    PdfApplication(int argc, char **argv);
    void setFileOpener(QObject *opener) {
        m_fileOpener = opener;
    }
protected:
    bool event(QEvent *e) override;

    QObject *m_fileOpener;
};


#endif //PDFREADER_PDFAPPLICATION_H
