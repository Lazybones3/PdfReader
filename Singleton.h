#ifndef PDFREADER_SINGLETON_H
#define PDFREADER_SINGLETON_H

#include <QMutex>

template <class T>
class Singleton {
public:
    static T* getInstance();

    static void deleteInstance();

    Singleton<T>(const Singleton<T>&) = delete;
    Singleton<T>(Singleton<T>&&) = delete;
    Singleton<T>& operator=(const Singleton<T>&) = delete;
    Singleton<T>& operator=(Singleton<T>&&) = delete;

protected:
    static T* _instance;

    Singleton<T>(){};
};

#endif //PDFREADER_SINGLETON_H
