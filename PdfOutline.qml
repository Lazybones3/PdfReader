import QtQuick
import QtQuick.Controls
import PdfViewComponents

Item {
    id: root

    signal pageSelected(var page)

    ScrollView {
        anchors.fill: parent

        TreeView {
            anchors.fill: parent
            // The model needs to be a QAbstractItemModel
            model: OutlineModel

            delegate: Item {
                id: treeDelegate

                implicitWidth: padding + label.x + label.implicitWidth + padding
                implicitHeight: label.implicitHeight * 1.5

                readonly property real indent: 20
                readonly property real padding: 5

                // Assigned to by TreeView:
                required property TreeView treeView
                required property bool isTreeNode
                required property bool expanded
                required property int hasChildren
                required property int depth

                TapHandler {
                    onTapped: treeView.toggleExpanded(row)
                }

                Text {
                    id: indicator
                    visible: treeDelegate.isTreeNode && treeDelegate.hasChildren
                    x: padding + (treeDelegate.depth * treeDelegate.indent)
                    anchors.verticalCenter: label.verticalCenter
                    text: "▸"
                    rotation: treeDelegate.expanded ? 90 : 0
                }

                Text {
                    id: label
                    x: padding + (treeDelegate.isTreeNode ? (treeDelegate.depth + 1) * treeDelegate.indent : 0)
                    width: treeDelegate.width - treeDelegate.padding - x
                    clip: true
                    elide: Text.ElideRight
                    text: model.title

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            root.pageSelected(model.dest)
                        }
                    }
                }
            }
        }
    }
}
