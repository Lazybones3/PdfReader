#ifndef PDFREADER_PDFVIEWERINITIALIZER_H
#define PDFREADER_PDFVIEWERINITIALIZER_H

#include <QObject>
#include <QtWebView/QtWebView>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "Singleton.cpp"
#include "FileUtils.h"
#include "WebSocketTransport.h"

namespace DIR {
    static const QString &path = "pdfjs/";
}

static QStandardItemModel* outlineModel;

class PdfViewerInitializer : public QObject, public Singleton<PdfViewerInitializer> {
    Q_OBJECT

    Q_PROPERTY(QString viewer READ viewer NOTIFY viewerChanged)

    friend class Singleton;

public:
    static void initialize();

    static QObject *getQmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine);

    Q_INVOKABLE static QByteArray pdfToBase64(const QString &path);

    Q_INVOKABLE bool initializeViewer(bool force=false);

    Q_INVOKABLE QString root(){
        return this->_root;
    }

    QString viewer(){
#ifdef _WIN32
        QString filePath = "file:///" + this->_viewer;
#else
        QString filePath = "file://" + this->_viewer;
#endif
        return filePath;
    }

    Q_INVOKABLE void setOutline(QJsonArray jsonArray);

    void parseOutlineItem(QStandardItem* item, QJsonObject* obj);

protected:
    QString _root;
    QString _viewer;
    bool _initialized;
    PdfViewerInitializer();

signals:
    void viewerChanged();
};


#endif //PDFREADER_PDFVIEWERINITIALIZER_H
