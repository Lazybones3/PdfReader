import QtQuick
import QtWebView
import QtWebSockets
import QtWebChannel
import PdfViewComponents

Item {
    id: root
//    width: parent.width
//    height: parent.height

    signal error(var message)
    signal pdfLoaded()
    signal viewerLoaded()
    signal textSelected(var txt)

    function load(path){
        console.log(path)
        var base64 = PdfViewerInitializer.pdfToBase64(path)
        webView.runJavaScript("loadDocument(\"%1\");".arg(base64))
    }

    function loadOutline() {
        webView.runJavaScript("loadOutline();")
    }

    function goToDestination(dest) {
        webView.runJavaScript("goToDestination(%1);".arg(dest))
    }

    property int pages: 0
    property int page: 1
    property var outline: []

    WebView {
        id: webView
        anchors.fill: parent
        // solve ERR_ACCESS_DENIED
        settings.allowFileAccess: true
        Component.onCompleted: {
            PdfViewerInitializer.viewerChanged.connect(function () {
                console.log(PdfViewerInitializer.viewer)
                webView.url = PdfViewerInitializer.viewer
            })
            PdfViewerInitializer.initializeViewer()
        }
    }

    QtObject {
        id: backend
        WebChannel.id: "backend"

        function error(message){
            root.error(message)
        }

        function viewerLoaded(){
            root.viewerLoaded();
        }

        function pdfLoaded(pages){
            root.pages = pages
            root.pdfLoaded()
        }

        function updateOutline(outline){
            // root.outline = outline
            PdfViewerInitializer.setOutline(outline)
        }

        function updatePage(page){
            console.log('updatePage:' + page)
            root.page = page
        }

        function getSelectedText(txt) {
            console.log('Text copied:', txt)
            root.textSelected(txt)
        }
    }

    WebChannel {
        id: channel
        registeredObjects: [backend]
    }

    WebSocketTransport {
        id: transport
    }

    WebSocketServer {
        id: server
        listen: true
        port: 54321

        onClientConnected: function (webSocket) {
            if(webSocket.status === WebSocket.Open) {
                console.log("WebSocket Server Open")
                channel.connectTo(transport)
                webSocket.onTextMessageReceived.connect(transport.textMessageReceive)
                transport.onMessageChanged.connect(webSocket.sendTextMessage)
            }
        }
    }
}
