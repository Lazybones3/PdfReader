#include "FileUtils.h"

FileUtils::FileUtils(QObject *parent) : QObject(parent) {

}

bool FileUtils::exists(const QString &path){
    if(path.isNull() || path.isEmpty())
        return false;
    return QFile::exists(path) || QFileInfo::exists(QUrl(path).toString()) || QFileInfo::exists(QUrl(path).toLocalFile());
}

bool FileUtils::copyDirs(const QString &sourceFolder, const QString &destFolder) {
    QDir sourceDir(sourceFolder);
    if (!exists(sourceFolder)) {
        qCritical() << sourceFolder << "is not exists!";
        return false;
    }

    QDir destDir(destFolder);
    if (!destDir.exists()) {
        QDir().mkpath(destFolder);
    }

    QStringList files = sourceDir.entryList(QDir::Files);
    for (int i = 0; i < files.count(); i++) {
        QString srcName = sourceFolder + "/" + files[i];
        QString destName = destFolder + "/" + files[i];
        QFile::copy(srcName, destName);
    }

    files.clear();
    files = sourceDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
    for(int i = 0; i< files.count(); i++){
        QString srcName = sourceFolder + "/" + files[i];
        QString destName = destFolder + "/" + files[i];
        copyDirs(srcName, destName);
    }

    return true;
}

bool FileUtils::removeDir(const QString &dirPath) {
    if(dirPath.compare("")==0) return false;

    if(QFile(dirPath).exists()) return QDir(dirPath).removeRecursively();
    return false;
}

QString FileUtils::joinPaths(const QStringList &paths){
    QString fullPath = "";

    for(const QString &path: paths){
        fullPath = QDir(fullPath).filePath(path);
    }

    return fullPath;
}
