#ifndef PDFREADER_FILEUTILS_H
#define PDFREADER_FILEUTILS_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QUrl>

class FileUtils : public QObject {
    Q_OBJECT
public:
    explicit FileUtils(QObject *parent = nullptr);

    static bool exists(const QString &path);

    static bool copyDirs(const QString &sourceFolder, const QString &destFolder);

    static bool removeDir(const QString &dirPath);

    static QString joinPaths(const QStringList &paths);
};


#endif //PDFREADER_FILEUTILS_H
