var backend;
window.onload = function() {
    var socket = new WebSocket("ws://127.0.0.1:54321");
    socket.onopen = function() {
        console.log("WebSocket Client Open")
        var webChannel = new QWebChannel(socket, function(channel) {
            backend = channel.objects.backend;
            connectPdfViewerSignals();
        });
    }

    socket.onerror = function(error) {
       alert("web channel error: " + error);
    };

    // 监听文本复制事件
    document.addEventListener('copy', function(event) {
        // 阻止默认的复制操作
        event.preventDefault();

        // 获取用户选择的文本
        const selectedText = window.getSelection().toString();

        // 执行自定义操作，例如将文本追加到剪贴板
        const clipboardData = event.clipboardData || window.clipboardData; // 跨浏览器兼容性
        clipboardData.setData('text/plain', selectedText);

        backend.getSelectedText(selectedText);
    });
}

function connectPdfViewerSignals() {
    PDFViewerApplication.pdfViewer.eventBus.on('pagerendered', function(evt){
        backend.pdfLoaded(PDFViewerApplication.pagesCount)
    });

    PDFViewerApplication.pdfViewer.eventBus.on('pagechanging', function(evt){
        backend.updatePage(evt.pageNumber)
    });

    PDFViewerApplication.pdfViewer.eventBus.on('erroroccurred', function(evt){
        backend.error(evt.msg)
    });

    backend.viewerLoaded()
}

function loadOutline() {
    PDFViewerApplication.pdfDocument.getOutline().then(outline => {
        backend.updateOutline(outline)
    })
}

function goToDestination(dest) {
    PDFViewerApplication.pdfLinkService.navigateTo(dest);
}

function loadDocument(base64){
    console.log('loadDocument')
    sleep(200).then(function() {
        var array = base64ToUint8Array(base64)

        // Load pdf document as an Uint8Array
        PDFViewerApplication.open(array);
    });
}

function base64ToUint8Array(base64){
    var raw = atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for(var i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array
}

function sleep(ms){
    return new Promise(function(resolve) {
        setTimeout(resolve, ms)
    });
}
